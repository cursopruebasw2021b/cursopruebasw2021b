Descripción del proyecto.

Instrucciones de la línea de comando

También puede subir archivos existentes desde su ordenador utilizando las instrucciones que se muestran a continuación.


Configuración global de Git

git config --global user.name "Jahir Saavedra" <br>
git config --global user.email "jah.saavedra@mail.udes.edu.co" <br>

Crear un nuevo repositorio

git clone https://gitlab.com/cursopruebasw2021b/cursopruebasw2021b.git <br>
cd cursopruebasw2021b <br>
git switch -c main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push -u origin main <br>

Push a una carpeta existente

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/cursopruebasw2021b/cursopruebasw2021b.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push -u origin main <br>

Push a un repositorio de Git existente

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/cursopruebasw2021b/cursopruebasw2021b.git <br>
git push -u origin --all <br>
git push -u origin --tags <br>

